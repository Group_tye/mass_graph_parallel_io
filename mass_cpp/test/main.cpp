#include "MASS.h"
#include "TestPlace.h"
#include "TestAgent.h"
#include "Timer.h"

using namespace std;

int main(int argc, char * argv[]) {
    if (argc != 10) {
        cerr << "Usage: ./test username password machinefile port nProc numThreads #testAgents simSize [show]" << endl;
        return -1;
    }

    char *arguments[4];
    arguments[0] = argv[1]; // username
    arguments[1] = argv[2]; // password
    arguments[2] = argv[3]; // machinefile
    arguments[3] = argv[4]; // port
    int nProc = atoi(argv[5]);
    int numThreads = atoi(argv[6]);
    int nAgents = atoi(argv[7]);    // # testAgents
    int simSize = atoi(argv[8]);     // simulation space
    bool show = (argv[9][0] == 's');


    Timer timer;

    MASS::init(arguments, nProc, numThreads);

    // initialize testPlaces
    char *msg = (char *) ("genericcreation\0");

    std::cout << "Creating TestPlaces now..." << std::endl;

    Places *testPlaces = new Places(1, "TestPlace", 1, msg, 16, 2, simSize, simSize);

    std::cout << "Finished creating TestPlaces!" << std::endl;

    void *showPtr = &show;
    testPlaces->callAll(TestPlace::init_, showPtr, sizeof(bool));

    std::cout << "Creating {" << nAgents << "} TestAgents now..." << std::endl;

    // initialize testAgents

    Agents *testAgents = new Agents(2, "TestAgent", msg, 16, testPlaces, nAgents);

    std::cout << "Done creating {" << nAgents << "} TestAgents now..." << std::endl;

    testAgents->callAll(TestAgent::init_, showPtr, sizeof(bool));
    
    testAgents->callAll(TestAgent::randomHop_);
    
    testAgents->callAll(TestAgent::printState_, showPtr, sizeof(bool));

    MASS::finish( );
    
    return 0;
}

