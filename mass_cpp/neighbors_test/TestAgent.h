//
// Created by sarah on 4/8/20.
//

#ifndef TESTAGENTS_H
#define TESTAGENTS_H

#include "Agent.h"
#include "MASS_base.h"


class TestAgent : public Agent {
public:

    static const int init_ = 0;
    static const int randomHop_ = 1;
    static const int printState_ = 2;

    // callMethod ------------------------------------------------------------------------------------------------------
    virtual void *callMethod(int functionId, void *arg) {

        switch (functionId) {

            case init_ :
                return init(arg);

            case randomHop_ :
                return randomHop();

            case printState_ :
                return printState(arg);

            default:
                return errorCallMethod();
        }
    }

    TestAgent(void *argument) : Agent(argument) {}

private:

    void* init(void *print);

    void* randomHop();

    void* printState(void *print);

    void* errorCallMethod();

    ostringstream convert;

};


#endif // TESTAGENTS_H
