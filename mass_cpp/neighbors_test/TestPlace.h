//
// Created by sarah on 4/8/20.
//

#ifndef TESTPLACE_H
#define TESTPLACE_H


#include <Place.h>
#include "MASS_base.h"

class TestPlace : public Place {
public:
    static const int init_ = 0;
    static const int testAgentsOnPlace_ = 1;

    static const int addRandomPlaceNeighbor_ = 2;
    static const int clearAndAddMoore_ = 3;
    static const int clearAndAddVNeumann_ = 4;
    static const int printPlaceNeighbors_ = 5;

    static const int prepToExchangeMyMessage_ = 6;
    static const int exchangeMyMessage_ = 7;
    static const int printMyInMessages_ = 8;

    virtual void *callMethod(int funcId, void *arg) {

        switch (funcId) {

            case init_ :
                return init(arg);

            case testAgentsOnPlace_ :
                return testingAgentsOnPlace(arg);

            case addRandomPlaceNeighbor_ :
                return addRandomPlaceNeighbor();

            case clearAndAddMoore_ :
                return clearAndAddMoore();

            case clearAndAddVNeumann_ :
                return clearAndAddVNeumann();

            case printPlaceNeighbors_ :
                return printPlaceNeighbors(arg);

            case prepToExchangeMyMessage_ :
                return prepToExchangeMyMessage();

            case exchangeMyMessage_ :
                return exchangeMyMessage();

            case printMyInMessages_ :
                return printMyInMessages(arg);
        }
    }

    TestPlace(void *arg) : Place(arg) {}

private:

    void *init(void *showPlaceId);

    void *testingAgentsOnPlace(void *print);

    void *addRandomPlaceNeighbor();

    void *clearAndAddMoore();

    void *clearAndAddVNeumann();

    void *printPlaceNeighbors(void *print);

    void *prepToExchangeMyMessage();

    void *exchangeMyMessage();

    void *printMyInMessages(void *print);

    // a default message to send to other places for testing ("Hello from your neighbor myX,myY")
    string myMessage;

    // new neighbor to add - relative x and y

    int newNeighborRelX;
    int newNeighborRelY;

    ostringstream convert;
};


#endif // TESTPLACE_H
