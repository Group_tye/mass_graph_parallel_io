//
// Created by sarah on 4/8/20.
//

#include "TestAgent.h"

extern "C" Agent *instantiate(void *argument) {
    return new TestAgent(argument);
}

extern "C" void destroy(Agent *object) {
    delete object;
}

void *TestAgent::init(void *print) {
    bool printOut = *(bool *) (print);
    if (printOut) {
        convert.str("");
        convert << "Hi World! I'm Agent " << agentId << " on Place " << index[0] << " , " << index[1] << endl;
        MASS_base::log(convert.str());
    }
    return nullptr;
}

/**
 * - assumes a 2D space
 * - call manageAll() after this function
 *
 * @return
 */
void *TestAgent::randomHop() {
    int relNewX = (rand() % place->size[0]) - index[0];
    int relNewY = rand() % place->size[1] - index[1];

    vector<int> offset;
    offset.emplace_back(relNewX);
    offset.emplace_back(relNewY);
    migrate(offset);
    return nullptr;
}

void *TestAgent::errorCallMethod() {
    convert.str("");
    convert << "ERROR: Invalid Function ID in Call Method for TestAgent " << agentId << endl;
    MASS_base::log(convert.str());
    return nullptr;
}

void *TestAgent::printState(void *print) {
    bool printOut = *(bool *) (print);
    if (printOut) {
        convert.str("");
        convert << "Agent " << agentId << " on Place " << index[0] << " , " << index[1] << endl;
        MASS_base::log(convert.str());
    }

    return nullptr;
}
