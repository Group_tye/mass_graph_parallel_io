//
// Created by sarah on 4/8/20.
//

#include "TestPlace.h"
#include<time.h>

extern "C" Place *instantiate(void *argument) {
    return new TestPlace(argument);
}

extern "C" void destroy(Place *object) {
    delete object;
}

void *TestPlace::init(void *showPlaceId) {
    std::ostringstream s;
    s << "Hello from your neighbor " << index[0] << " , " << index[1] << " !";
    std::string initMessage(s.str());

    myMessage.append(initMessage);

    newNeighborRelX = -1;
    newNeighborRelY = -1;

    bool print = *(bool *) (showPlaceId);
    if (print) {
        convert.str("");
        convert << "Hello World! I'm TestPlace " << index[0] << " , " << index[1] << " ,and I have " << agents.size() <<
                " agents and " << getNeighbors().size() << " neighboring TestPlaces." << endl;
        MASS_base::log(convert.str());
    }
    return nullptr;
}

/*
 * Note : tests assume 2D space
 */

void *TestPlace::testingAgentsOnPlace(void *print) {
    bool printOut = *(bool *) (print);
    if (printOut) {
        convert.str("");
        convert << "Testing Place's Agent Count : Place " << index[0] << " , " << index[1] << " has " << agents.size()
                << " agents " << endl;
        MASS_base::log(convert.str());
    }
    return nullptr;
}

void *TestPlace::addRandomPlaceNeighbor() {

    int placeID = index[0]*size[0] + index[1];
    srand(time(0) + placeID);

    newNeighborRelX = rand() % size[0] - index[0];
    newNeighborRelY = rand() % size[1] - index[1];

        convert.str("");
        convert << "Place " << index[0] << " , " << index[1] << "is adding the neighbor :";
        convert << newNeighborRelX << " , " << newNeighborRelY << endl;
        MASS_base::log(convert.str());

    int newNeighbor[2] = { newNeighborRelX, newNeighborRelY };
    addNeighbor(newNeighbor, 2);
    return nullptr;
}

void *TestPlace::printPlaceNeighbors(void *print) {
    bool printOut = *(bool *) (print);
    if (printOut) {
        convert.str("");
        convert << "Testing this Place's neighbors : Place " << index[0] << " , " << index[1] << " has the following "
                                                                                                 "neighbors :" << endl;
        vector<int *> myNeighbors = getNeighbors();
        for (int i = 0; i < myNeighbors.size(); i++) {
            convert << "  " << myNeighbors[i][0] << " , " << myNeighbors[i][1] << "  ";
        }
        convert << endl;
        MASS_base::log(convert.str());
    }
    return nullptr;
}

void *TestPlace::clearAndAddMoore() {
    addNeighbors(MOORE2D);
    return nullptr;
}

void *TestPlace::clearAndAddVNeumann() {
    addNeighbors(VON_NEUMANN2D);
    return nullptr;
}

/**
 * prepares TestPlaces' in and out message boxes for an exchangeAll of string myMessage
 * @return
 */
void *TestPlace::prepToExchangeMyMessage() {
    // clear out message
    outMessage = NULL;
    outMessage_size = 0;

    inMessage_size = sizeof(myMessage);
    return nullptr;
}

void *TestPlace::exchangeMyMessage() {
    string *toSend = &myMessage;
    return toSend;
}

void *TestPlace::printMyInMessages(void *print) {
    bool printOut = *(bool *) (print);
    if (printOut) {
        convert.str("");
        convert << "Place " << index[0] << " , " << index[1] << " has the following in_messages :" << endl;

        for (int i = 0; i < inMessages.size(); i++) {
            convert << "    " << inMessages[i] << endl;
        }
        convert << "--------------------------- end of in messages ---------------------------" << endl;
        MASS_base::log(convert.str());
    }
    return nullptr;
}
